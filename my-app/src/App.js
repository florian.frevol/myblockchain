import logo from './logo.svg';
import './App.css';
import './Firebase'
import SignIn from "./components/SignIn";
import SignUp from "./components/SignUp";
import Application from "./components/Application";
import UserProvider from "./providers/UserProvider";
import ProfilePage from "./components/ProfilePage";
import { UserContext } from "./providers/UserProvider";

function App() {

  localStorage.setItem('amount', "");
  localStorage.setItem('fee', "");
  localStorage.setItem('from', "");
  localStorage.setItem('to', "");
  localStorage.setItem('privateKey', "");


  return (
    <div className="App">
      <UserProvider>
         <Application/>
      </UserProvider>
    </div>
  );
}

export default App;
