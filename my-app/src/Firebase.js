import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import Firebase from 'firebase'
import LogRocket from 'logrocket';

const firebaseConfig = {
  apiKey: "AIzaSyCVDaG1K0eaGJVkJIFtUxo1V_QHRAW9YNI",
  authDomain: "blockchain-22885.firebaseapp.com",
  databaseURL: "https://blockchain-22885.firebaseio.com",
  projectId: "blockchain-22885",
  storageBucket: "blockchain-22885.appspot.com",
  messagingSenderId: "1082293080186",
  appId: "1:1082293080186:web:a2bd28827a2745fbbbebc3"
};

LogRocket.init('ttur5w/blockchain');
firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();
export const firestore = firebase.firestore();
// export const database = Firebase.database();
// export const database = firebase.database();

export const generateUserDocument = (user, additionalData) => {
  if (!user) return;

  console.log("uuuuu")
  const userRef = firestore.doc(`users/${user.uid}`);
  const snapshot = userRef.get();
  if (!snapshot.exists) {
    const { email, displayName, photoURL } = user;
    // console.log("eee")
    try {
      // console.log("aaa")
      // var newPostKey = firebase.database().ref().child('users').push().key;
      // var data = {
      //   displayName: displayName,
      //   email: email,
      //   photoURL, photoURL,
      //   ...additionalData
      // }
      // var updates = {};
      // updates[newPostKey] = data;
      // firebase.database().ref('users').update(updates)

      // .set(({
      //   displayName,
      //   email,
      //   photoURL,
      //   ...additionalData
      // }))





      userRef.set({
        displayName,
        email,
        photoURL,
        ...additionalData
  
      });
    } catch (error) {
      console.error("Error creating user document", error);
    }
  }
  return getUserDocument(user.uid);
};

const getUserDocument = async uid => {
  if (!uid) return null;
  try {
    const userDocument = await firestore.doc(`users/${uid}`).get();
    
    return {
      uid,
      ...userDocument.data()
    };
  } catch (error) {
    console.error("Error fetching user", error);
  }
};



  
  // if (!snapshot.exist) {
  //   try {
  //     await networkRef.add({
  //       BLOCKCHAIN_LENGTH: "Florian",
  //       NODE_NR: "Frevol"
  //     });
  //   } catch (error) {
  //     console.error("Erreur creting new node to network")
  //   }
  // }


