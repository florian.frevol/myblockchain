import React, {useContext, useState, useEffect} from 'react'
import {UserContext} from "../providers/UserProvider";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {TabPanel, a11yProps, useStyles} from '../components/Table'
import Economy from '../components/Economy'

const firestore = firebase.firestore();

const EconomyScreen = () => {
    const user = useContext(UserContext)

    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    useEffect(() => {

    }, [])

    return (
        <div>
            <div style={{height: 100}}></div>
            <div className="section">
            <div className="section-title">
                       ECONOMY
                    </div>
                <div className={classes.root}>
                    <Tabs 
                        orientation="vertical"
                        variant="scrollable"
                        value={value}
                        onChange={handleChange}
                        aria-label="Vertical tabs example"
                        className={classes.tabs}
                    >
                        <Tab label="Goods / Services" {...a11yProps(0)} />
                    </Tabs>
                    <TabPanel value={value} index={0} >
                        <Economy/>
                    </TabPanel>
                </div>
               </div>
        </div>
    )
}

export default EconomyScreen;