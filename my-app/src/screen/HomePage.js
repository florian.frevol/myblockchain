import React from "react";
import { auth } from "../Firebase";
import '../Styles/homepage.css'
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Wallet from './Wallet'
import LocalNode from './LocalNode'
import EconomyScreen from './EconomyScreen'
import MyLocalMinerSoftware from './MyLocalMinerSoftwareScreen'

export default function HomePage() {

    return (
        <div>
            <Wallet/>
            <LocalNode/>
            <MyLocalMinerSoftware />
            <EconomyScreen />
        </div>
    );
}
