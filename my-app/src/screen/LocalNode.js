import React, { useContext, useState, useEffect } from 'react'
import { UserContext } from "../providers/UserProvider";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import Network from '../components/Network'
import TransactionGossip from '../components/TransactionGossip'
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { TabPanel, a11yProps, useStyles } from '../components/Table'
import Mempool from '../components/Mempool'
import MyPersonalBlockchain from '../components/MyPersonalBlockchain'
import MyPersonalBlockchainOldVersion from '../components/MyPersonalBlockchainOldVersion'

const firestore = firebase.firestore();

const LocalNode = () => {
    const user = useContext(UserContext)

    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };
    useEffect(() => {

    }, [])

    return (
        <div>
            <div style={{ display : "flex"}}></div>
            <div className="section">
                <div className="section-title">
                    MY LOCAL NODE SOFTWARE
                    </div>
                <div className={classes.root}>
                    <Tabs
                        orientation="vertical"
                        variant="scrollable"
                        value={value}
                        onChange={handleChange}
                        aria-label="Vertical tabs example"
                        className={classes.tabs}
                    >
                        <Tab label="Network" {...a11yProps(0)} />
                        <Tab label="My Personal BlockChain" {...a11yProps(1)} />
                        <Tab label="New Transactions Gossip" {...a11yProps(2)} />
                        <Tab label="Mempool" {...a11yProps(3)} />
                    </Tabs>
                    <TabPanel value={value} index={0} >
                        <Network />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <MyPersonalBlockchain />
                        <MyPersonalBlockchainOldVersion />
                    </TabPanel>
                    <TabPanel value={value} index={2}>
                        <TransactionGossip />
                    </TabPanel>
                    <TabPanel value={value} index={3}>
                        <Mempool />
                    </TabPanel>
                </div>
            </div>


            {/* <Network/>
            <TransactionGossip/> */}
        </div>
    )
}

export default LocalNode;