import React, {useContext, useState, useEffect} from "react";
import { auth } from "../Firebase";
import '../Styles/homepage.css'
import {UserContext} from "../providers/UserProvider";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { Button } from "@material-ui/core";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import sha256 from 'js-sha256';
import {TabPanel, a11yProps, useStyles} from '../components/Table';
import BlockChainKeys from '../components/BlockchainKey'
import TransactionInititation from '../components/TransactionInitiation'

const Wallet = () => {
    const classes = useStyles();
    const user = useContext(UserContext)
    const [value, setValue] = useState(0);
    
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return (
        <div style={{height: 700}} className="biggestContainer">
            <div className="container">
                <div className="bigblue">
                    BlockChain Simulator
                </div>
                <button className="quitbtn" onClick={() => { auth.signOut() }}>Sign out</button>
            </div>
            <div className="section">
                <div className="section-title">
                    MY LOCAL WALLET SOFTWARE
                    </div>

                <div className={classes.root}>
                    <Tabs
                        orientation="vertical"
                        variant="scrollable"
                        value={value}
                        onChange={handleChange}
                        aria-label="Vertical tabs example"
                        className={classes.tabs}
                    >
                        <Tab label="My Blockchain keys" {...a11yProps(0)} />
                        <Tab label="Transaction Initiation" {...a11yProps(1)} />
                    </Tabs>
                    <TabPanel value={value} index={0} >
                            <BlockChainKeys />
                    </TabPanel>
                    <TabPanel value={value} index={1}>
                        <TransactionInititation />
                </TabPanel>
                </div>
            </div>

            <div>
            </div>
        </div>
    )
}

export default Wallet;