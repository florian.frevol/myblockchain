import React, { useContext, useState, useEffect } from 'react'
import { UserContext } from "../providers/UserProvider";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import "../Styles/Components/MyBlockChain.css"
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { Crypt, RSA } from 'hybrid-crypto-js';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const database = Firebase.database();

const useStyles = makeStyles({
    root: {
        width: '100%',
        outline: "none"
    },
    container: {
        maxHeight: 50,
        maxWidth: 1300,
        outline: "none",
        border: 0,
    },
    modal: {
        textAlign: 'center',
        width: '35vw',
        backgroundColor: 'white',
        opacity: 0.8,
        outline: 0, // add / remove
    }
});

var MyPersonalBlockchain = () => {
    const classes = useStyles();

    const user = useContext(UserContext)
    const [blockChain, setBlockChain] = useState([]);
    const [blockChainBolean, setBlockChainBolean] = useState(true)

    useEffect(() => {
        var ref = firebase.database().ref("nodeSoftware/network");
        ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function (snapshot) {
            console.log(snapshot.key)
            var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/myblockchains");
            starCountRef.on('value', (snapshot) => {

                var newData = [];
                snapshot.forEach((snapshot) => {
                    newData.push(snapshot.val());
                })
                console.log(newData)
                setBlockChain(newData);
                console.log(newData);
                if (newData.length > 0) {
                    setBlockChainBolean(false)
                }
            })
        });

    }, [])
    const AddToMempool = () => {   // Ajouter une transaction au mempool
        var ref = firebase.database().ref("nodeSoftware/network");
        var crypt = new Crypt();
        var newPostKey = "";
        var newData = {
            BLOCK: 460,
            NONCE: 0,
            DATA: {
                AMOUNT: 10000,
                FEE: 0,
                FROM: "TRUc"
            },
            PREV: 110,
            HASH: 102,
            UUID: 1111111,
            // ENCRYPTED: crypt.encrypt(row.to, row.uuid, signature), 
        }
        ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function (snapshot) {
            newPostKey = firebase.database().ref().child('nodeSoftware/network/' + snapshot.key + "/myblockchains").push().key;
            console.log(snapshot.val())
            console.log(snapshot.key)
            var updates = {};
            updates[newPostKey] = newData;
            database.ref('nodeSoftware/network/' + snapshot.key + "/myblockchains").update(updates)
            console.log("test")
            // var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/transactionsGossip");
            // starCountRef.orderByChild("UUID").equalTo(1111111).on('child_added', function(newsnapshot2) {
            //   firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/transactionsGossip/" + newsnapshot2.key).remove();
            // })
        });
    }
    //   AddToMempool()

    console.log(blockChainBolean)
    return (
        <div className="container" style={{ width: 1300, height: 400, borderRadius: 20, flexDirection: "column", justifyContent: "flex-start", alignItems: "flex-start" }}>
            <Button variant="contained" color="primary" onClick={() => AddToMempool()} >Send to mempool</Button><div style={{ marginBottom: 20 }}>
                NEW BLOCK(S) TO VERIFY BEFORE INSERTION IN MY BLOCKCHAIN<br></br>
                There are no blocks to verify!
            </div>
            <div className="line">
                <br></br>
            </div>
            <div>
                MY BLOCKCHAIN
            </div>
            {blockChainBolean
                ? <div style={{ marginTop: 20 }}>
                    <br></br>
                    You have no blocks in your blockchains!
                </div>
                :
                <div>
                    {blockChain.map((row) => (
                        <div style={{ flexDirection : "row", backgroundColor: "grey", height: 20 }}>
                            wsh
                            <div style={{ backgroundColor: "grey", height: 20 }}>
                                <TableContainer className={classes.container}>
                                    <Table stickyHeader aria-label="sticky table">
                                        <TableHead>

                                            <TableRow>
                                                <TableCell align='left'>Amount</TableCell>
                                                <TableCell align='left'>Fee</TableCell>
                                                <TableCell align='left'>From</TableCell>
                                                <TableCell align='left'>To</TableCell>
                                                <TableCell align='left'>Signature</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                                <TableRow>
                                                    <TableCell align='left'>{row.AMOUNT}</TableCell>
                                                    <TableCell align='left'>{row.FEE}</TableCell>
                                                    <TableCell align='left'>{row.FROM}</TableCell>
                                                    <TableCell align='left'>{row.TO}</TableCell>
                                                    <TableCell align='left'>{row.SIGNATURE}</TableCell>
                                                </TableRow>
                                        </TableBody>
                                    </Table>

                                </TableContainer>
                            </div>
                        </div>
                    ))}

                </div>}
        </div>
    )
}


export default MyPersonalBlockchain