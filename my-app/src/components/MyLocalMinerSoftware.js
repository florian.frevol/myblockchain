import React, { useContext, useState, useEffect } from 'react'
import Button from '@material-ui/core/Button';
import { UserContext } from "../providers/UserProvider";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Crypt, RSA } from 'hybrid-crypto-js';
import {FaHashtag} from 'react-icons/fa'
import crypto from 'crypto' 


// import {checkNodeNetwork} from './Network'

const firestore = firebase.firestore();
const database = Firebase.database();

const useStyles = makeStyles({
  root: {
    width: '100%',

    // outline: "none"
  },
  container: {
    // maxHeight: 440,
    // maxWidth: 1300,
    // outline: "none",
    border: 0,
  },
  modal: {
    textAlign: 'center',
    width: '35vw',
    backgroundColor: 'white',
    opacity: 0.8,
    outline: 0, // add / remove
  },
  input: {
    width: "100%"
  }
});

const MinerSoftware = () => {
    const classes = useStyles();
    const user = useContext(UserContext)
    const [dataMinerSoftware, setdataMinerSoftware] = useState([]);
    const [block, setBlock] = useState("");
    const [compute, setCompute] = useState(false);
    const [seconds, setSeconds] = useState(0);
    const [computeHash, setComputeHash] = useState(""); 
    const [mined, setMined] = useState(false);
    const [dataCompute, setDataCompute] = useState({});
    const [computeHashSend, setComputeHashSend] = useState("");
    const [previousHashSend, setPreviousHashSend] = useState("");
    const [dispayPrevioushash, setDisplayPrevioushash] = useState(false);
    const [nonce, setNonce] = useState(0);


 
    // const checkTransactionFirebase = (UUID, key) => {
    //   // console.log(UUID)
    //   return new Promise(async (resolve, reject) => {
    //     var bool = true;
    //     var refNetwork = firebase.database().ref("nodeSoftware/verifyTransaction/" + key + "/transactions")
    //     refNetwork.orderByValue().equalTo(UUID).on("child_added", (snapshot) => {
    //       console.log("woula")
    //       bool = false;
    //       reject()
    //     });
    //     if (bool)
    //       resolve()
    //     else
    //       reject()
    //   })
    // }

    const previousHash = () => {

      return new Promise( async (resolve, reject) => {
        var refNetwork = firebase.database().ref("nodeSoftware/verifyTransaction");
        var stringpreviousHash = "0000000000000000000000000000000000000000000000000000000000000000";
        console.log("alla akbar")
        await refNetwork.orderByChild("user_id").equalTo(user.uid).limitToLast(1).once("child_added", (snapshot) => {
          stringpreviousHash = snapshot.val().hash;
          console.log(snapshot.val())
        }).then(() => {
          resolve(stringpreviousHash)

        }).catch((err) => console.log(err))
        console.log('caca')
        resolve( stringpreviousHash)
        // return (stringpreviousHash);

      })
    }

    const resetBlock = () => {
      var ref = firebase.database().ref("nodeSoftware/network");
      ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(firstsnapshot) {
        var starCountRef = firebase.database().ref('nodeSoftware/network/' + firstsnapshot.key + "/MinerSoftware");
          starCountRef.on('value', (row) => {
            var newsnapshot;
            var newPostKey;
            var updates = {};
            console.log("je suis une grosse pute")
            row.forEach((snapshot) => {
              newsnapshot = snapshot.val();
              newPostKey = firebase.database().ref('nodeSoftware/network/' + firstsnapshot.key + "/mempool").push().key;
              updates[newPostKey] = newsnapshot;
              // console.log("cacacac")
              firebase.database().ref('nodeSoftware/network/' + firstsnapshot.key + "/mempool").update(updates)
              updates = {};
            })
      })
      starCountRef.on('value', (row) => {
          row.forEach((snapshot) => {
          firebase.database().ref('nodeSoftware/network/' + firstsnapshot.key + "/MinerSoftware/" + snapshot.key).remove();
        })
  })
      });

      
    }

    const createNewBlock = (data) => {
      var newPostKey = firebase.database().ref("nodeSoftware/verifyTransaction").push().key;
      console.log("caca")
      var updates = {};
      updates[newPostKey] = data;
      database.ref("nodeSoftware/verifyTransaction").update(updates)

      // console.log(data)
    }

    const newCheck = (data) => {
      var bool = false;
      var stringError = "";

      return new Promise((resolve, reject) => {
        firebase.database().ref("nodeSoftware/verifyTransaction").once("value", (snapshot) => {

            data.transactions.forEach((transaction) => {
              snapshot.forEach((row) => {
                row.val().transactions.forEach((transactionFirebase) => {
                  console.log(row.val().transactions)
                    if (transactionFirebase == transaction) {
                      alert("Le block a déjà était validé")
                      stringError = "une des transactions existe déjà ";
                      bool = true;
                    }
                })
                if (row.val().block == data.block) {
                  alert("Le block existe déjà")
                  stringError = "Le block existe déjà";
                  bool = true;
                }
                })
  
            })
          if (bool == false) {
            createNewBlock(data);
            alert("Block successfully vaidate")
            resolve({message: "data write on serveur", statut: 200})
          } else {
            reject({message: stringError, statut: 400});
          }
      })

      })
  }

    const sendBlock = async () => {

      // var id_transactions = [];
      // var stringBlock = "";
      // transactions.map((row) => {
      //   id_transactions.push(row.UUID);
      //   stringBlock += row.UUID;
      // })

      // setCompute(true)
      // // previous_hash();
      // var data = {
      //     block: block,
      //     user_id: user.uid,
      //     nonce: 56,
      //     previous_hash: await previousHash(),
      //     transactions: id_transactions,
      //     hash:  crypto.createHash('sha1').update(stringBlock).digest('hex')
      // }

      console.log(dataCompute)
      newCheck(await dataCompute)
      .then((res) => {console.log(res)})
      .catch((err) => {console.log(err)})
      
      
      
    }
    
    const mineBlock = async (transactions, block) => {
      var id_transactions = [];
      var stringBlock = "";
      transactions.map((row) => {
        id_transactions.push(row.UUID);
        stringBlock += row.UUID;
      })
      
      setCompute(true)
      // previous_hash();
      var data = {
        block: block,
        user_id: user.uid,
        nonce: 56,
        previous_hash: previousHashSend,
        transactions: id_transactions,
        hash:  crypto.createHash('sha1').update(stringBlock).digest('hex')
    }
    setMined(false);
    setSeconds(0);
    
      var rand = Math.floor(Math.random() * (10 - 5 + 1) + 5)
      console.log('Wait for ' + rand + ' seconds');
      await setTimeout( async () => {
        data.nonce = Math.round(seconds)
        setDataCompute(data)
        setNonce(Math.round(seconds))
        setMined(true);
        setCompute(false);
        console.log(data)
        setComputeHashSend(await data.hash);
        // setComputeHash(dataCompute.hash);
        console.log("j'ai faim")
        // console.log(i);
      }, Math.floor(rand * 1000));


    }





    useEffect(() => {
      var ref = firebase.database().ref("nodeSoftware/network");
      ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(snapshot) {
        // console.log(snapshot.key)
        var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/MinerSoftware");
          starCountRef.on('value', (snapshot) => {
  
        var newData = [];
          snapshot.forEach((snapshot) => {
            newData.push(snapshot.val());
          })
          setdataMinerSoftware(newData);
      })
      });
      
      if (compute)
        console.log("woula")
      else
        console.log("pas woula")

        const timer = setTimeout(() => {
          console.log('This will run after 1 second!')
        }, 1000);
        return () => clearTimeout(timer);


    }, [])
  

    function Hash() {
      var result           = '';
      var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
      var charactersLength = characters.length;
      for ( var i = 0; i < 20; i++ ) {
         result += characters.charAt(Math.floor(Math.random() * charactersLength));
      }

      result = crypto.createHash('sha1').update(result).digest('hex')

      return result;
   }
   


    useEffect(() => {
      const interval = setInterval(() => {
        setSeconds(seconds => seconds + 0.15)
        setComputeHash(Hash())

        // if (compute == 1) {
            // console.log('This will run every second!' + compute);
        
          // }
        }, 20);
        return () => clearInterval(interval);
    }, []);




    return (
      <div style={{backgroundColor: mined ? "#91ff35" : "white", padding: 40}}>
        <div style={{display: "flex", alignSelf: "flex-start"}}>Block</div>
        <div style={{display: "flex", flexDirection: "row", marginBottom: 30}}><div style={{width: "5%", backgroundColor: "#a8ada9"}}> <FaHashtag/>  </div>  <input value={block} onChange={(event) => setBlock(event.target.value)} className={classes.input}></input> </div>
        <div style={{display: "flex", alignSelf: "flex-start"}}>Nonce</div>
        <input style={{marginBottom: 30}} className={classes.input} value={compute ? Math.round(seconds) : (mined ? nonce : 0)}></input>
        <div style={{display: "flex", alignSelf: "flex-start"}}>Data</div>

        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
  
                <TableRow>
                  <TableCell align='left'>Amount</TableCell>
                  <TableCell align='left'>Fee</TableCell>
                  <TableCell align='left'>From</TableCell>
                  <TableCell align='left'>To</TableCell>
                  <TableCell align='left'>Signature</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {dataMinerSoftware.map((row) => (
                  <TableRow>
                    <TableCell align='left'>{row.AMOUNT}</TableCell>
                    <TableCell align='left'>{row.FEE}</TableCell>
                    <TableCell align='left'>{row.FROM}</TableCell>
                    <TableCell align='left'>{row.TO}</TableCell>
                    <TableCell align='left'>{row.SIGNATURE}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
  
          </TableContainer>
        </Paper>
        <div style={{display: "flex", alignSelf: "flex-start", marginTop: "30px"}} >Prev Hash</div>
        <input style={{marginBottom: 30}} className={classes.input}  disabled value={previousHashSend}></input>
        <div style={{display: "flex", alignSelf: "flex-start"}}>Hash</div>
        <input style={{marginBottom: 30}} className={classes.input} value={compute ? computeHash : (mined ? computeHashSend : "")} disabled></input>

        <div style={{marginBottom: 30, display: "flex", alignItems: "flex-start"}}><Button variant="contained" color="primary" onClick={() => mineBlock(dataMinerSoftware, block)} disabled={previousHashSend != "" ? false : true}>Mine</Button></div>
        <button style={{width: "100%", height: "30px", backgroundColor: "#6C757D", color: "white", marginBottom: "15px"}} onClick={() => resetBlock()}>RESET BLOCK</button>
        {/* <Button variant="contained" color="primary" style={{width: "100%"}} onClick={() => resetBlock()}>Reset block</Button> */}

        <button style={{width: "100%", height: "30px", backgroundColor: "#6C757D", color: "white", marginBottom: "15px"}} onClick={async () => {
          setDisplayPrevioushash(true); 
          var t = false;
          previousHash().then((res) => {
            t = true;
            console.log("then: " + res)
            setPreviousHashSend(res)
          })
          console.log("ezf")
          if (!t)
            setPreviousHashSend("0000000000000000000000000000000000000000000000000000000000000000")
          // var prev = await previousHash() ;
          // setPreviousHashSend( await prev) 
          
          }}>INSERT PREVIOUS BLOCK HASH</button>
        <Button variant="contained" color="primary" style={{width: "100%"}} disabled={!mined} onClick={() => sendBlock()}>SEND BLOCK TO THE NETWORK</Button>

        <div>
        </div>

      </div>

      
    )
  }

  export default MinerSoftware;