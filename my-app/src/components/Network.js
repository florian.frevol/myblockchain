import React, {useContext, useState, useEffect} from 'react'
import Button from '@material-ui/core/Button';
import {UserContext} from "../providers/UserProvider";
import firebase from "firebase/app";
import Firebase from 'firebase'
import '../Styles/homepage.css'
import "firebase/auth";
import "firebase/firestore";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';


const firestore = firebase.firestore();
const database = Firebase.database();

const useStyles = makeStyles({
  root: {
      width: '100%',
      outline: "none"
    },
    container: {
      maxHeight: 440,
      outline: "none",
      border: 0,
    },
    modal: {
      textAlign: 'center',
      width: '35vw',
      backgroundColor: 'white',
      opacity: 0.8,
      outline: 0, // add / remove
    }
});
  
const addNodeToNetwork = (user) => {

    return new Promise(async (resolve, reject) => {

      var bool = false;
      var ref = firebase.database().ref("nodeSoftware/network");
      ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(snapshot) {
        bool = true;
      });

      if (!bool) {
      var newPostKey = firebase.database().ref().child('nodeSoftware/network').push().key;
      var newData = {
                  BLOCKCHAIN_LENGTH: 0,
                  ID_USER: user.uid,
                  timestamp: new Date()
  }
      var updates = {};
      updates[newPostKey] = newData;

      database.ref("nodeSoftware/network").update(updates).then(
        resolve()
      )
      }
    })
  }

  // const AddNetworkUser = (user) => {
  //   const { email, displayName, photoURL } = user;
  //     console.log("aaa")
  //     var newPostKey = firebase.database().ref().child('users').push().key;
  //     var data = {
  //       USER_ID: user.uid,
  //       TransactionGossip: {}
  //     }
  //     var updates = {};
  //     updates[newPostKey] = data;
  //     firebase.database().ref('users').update(updates)
  // }
  

function Network() {
  const classes = useStyles();
  const user = useContext(UserContext);
  const [nodeConnection, setNodeConnection] = useState([]);


  useEffect(() => {
    var starCountRef = firebase.database().ref('nodeSoftware/network');
    starCountRef.on('value', (snapshot) => {
      var newData = [];
      var val = 1;
      var test = {};
      snapshot.forEach((snapshot) => {
        test = snapshot.val();
        test.id = val++;
        newData.push(test);
      });
      setNodeConnection(newData);
      console.log(nodeConnection);
    });
  }, []);

  return (
    <div>
      <div>
        {false ?
          <Button variant="contained" color="primary" disabled> Already on Network</Button> :
          <Button variant="contained" color="primary" onClick={() => {
            addNodeToNetwork(user).then((bool) => { })
              .catch(err => console.log(err));

          } }>Join Network</Button>}
      </div>

      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                <TableCell align='left'>NODE_NR</TableCell>
                <TableCell align='left'>BLOCKCHAIN LENGTH</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {nodeConnection.map((row) => (
                <TableRow>
                  <TableCell align='left'>{row.id}</TableCell>
                  <TableCell align='left'>{row.BLOCKCHAIN_LENGTH} Blocks</TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>

        </TableContainer>
      </Paper>
    </div>
  );
}

export default Network;
// export default checkNodeNetwork;