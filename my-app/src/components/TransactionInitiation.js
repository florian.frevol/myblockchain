import React, {useContext, useState, useEffect} from "react";
import { auth } from "../Firebase";
import '../Styles/homepage.css'
import {UserContext} from "../providers/UserProvider";
import { Button } from "@material-ui/core";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import sha256 from 'js-sha256';
import {TabPanel, a11yProps, useStyles} from './Table';
import { Crypt, RSA } from 'hybrid-crypto-js';
import { v4 as uuidv4 } from 'uuid';

const database = Firebase.database();

const addTansactionGossip = (user, amount, fee, from, to, privateKey, uuid) => {
    return new Promise( (resolve, reject) => {
        var ref = firebase.database().ref("nodeSoftware/network");
        var crypt = new Crypt();
        var newPostKey = "";
        var signature = crypt.signature(privateKey, uuid);
        var newData = {
                AMOUNT: amount,
                BALANCE_OK: "2",
                SIGNATURE_OK: "2",
                FEE: fee,
                FROM: from,
                TO: to,
                SIGNATURE: signature,
                UUID: uuid,
                // ENCRYPTED: crypt.encrypt(to, uuid, signature), 
        }
        ref.orderByChild("ID_USER").on("child_added", function(snapshot) {
            newPostKey = firebase.database().ref().child('nodeSoftware/network/' + snapshot.key + "/transactionsGossip").push().key;
            console.log(snapshot.val())
            console.log(snapshot.key)
            var updates = {};
            updates[newPostKey] = newData;
            database.ref('nodeSoftware/network/' + snapshot.key + "/transactionsGossip").update(updates)

        });
                }
                    ) 
            
}


const TransactionInitiation = () => {
    const user = useContext(UserContext)
    const [amount, setAmount] = useState(localStorage.getItem('amount'));
    const [fee, setFee] = useState(localStorage.getItem("fee"));
    const [from, setFrom] = useState(localStorage.getItem("from"));
    const [to, setTo] = useState(localStorage.getItem("to"));
    const [privateKey, setPrivateKey] = useState(localStorage.getItem("privateKey"));
    const [signature, setSignature] = useState("");
    const [uuid, setUuid] = useState("");
    var crypt = new Crypt();


    return (
        <div>

            <div style={{display: "flex", flexDirection: "row", width: "100%", alignItems: "center", justifyContent: "center"}}>
            <div style={{height: 120}}>Transaction</div>
            <label>
             $
             <input type="text" value={amount} onChange={(event) => {setAmount(event.target.value); localStorage.setItem('amount', event.target.value)}} />        </label> 
             <label> 
             Fee
             <input type="text" value={fee} onChange={(event) => {setFee(event.target.value); localStorage.setItem('fee', event.target.value)}} />        </label>
             <label>
             From
             <input type="text" value={from} onChange={(event) => {setFrom(event.target.value); localStorage.setItem('from', event.target.value)}} />        </label>
             <label>
             To
             <input type="text" value={to} onChange={(event) => {setTo(event.target.value); localStorage.setItem('to', event.target.value)}} />        </label>

            </div>
            <label>
            <div style={{height: 10}}></div>
             Private Key
             <input type="text" value={privateKey} onChange={(event) => {setPrivateKey(event.target.value); localStorage.setItem('privateKey', event.target.value)}} />        </label>
             <Button variant="contained" color="primary" onClick={() => { 
                        setUuid(uuidv4());
                        setSignature(crypt.signature(privateKey, uuid))
           
                   }}>Sign</Button>
                   <div style={{height: 50}}></div>
             <label>
             Signature
             <input type="text" value={signature} disabled />        </label>
             

                   <Button variant="contained" color="primary" onClick={() => { 
                   addTansactionGossip(user, amount, fee, from, to, privateKey, uuid); setAmount(""); setFee(""); setFrom(""); setTo(""); setPrivateKey("");
           
                   }}>Publish transaction to the network</Button>
        </div>
    )
}

export default TransactionInitiation;