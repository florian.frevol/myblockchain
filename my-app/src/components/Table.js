import React from "react";
import { auth } from "../Firebase";
import '../Styles/homepage.css'
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

export function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`vertical-tabpanel-${index}`}
            aria-labelledby={`vertical-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box p={3}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
};

export function a11yProps(index) {
    return {
        id: `vertical-tab-${index}`,
        'aria-controls': `vertical-tabpanel-${index}`,
    };
}

export const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        //backgroundColor: theme.palette.background.paper,
        display: 'flex',
        width: '100%',
        outline: "none",
    },
    container: {
        maxHeight: 400,
        maxWidth: 1300,
        outline: "none",
        border: 0,
    },
    modal: {
        textAlign: 'center',
        width: '35vw',
        backgroundColor: 'white',
        opacity: 0.8,
        outline: 0, // add / remove
      },
    tabs: {
        borderRight: `1px solid ${theme.palette.divider}`,
    },
}));