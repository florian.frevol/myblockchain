import React, { useState } from "react";
import { Link } from "@reach/router";
import {auth, generateUserDocument} from '../Firebase'
import '../Styles/Components/signup.css'
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import TransactionGossip from "./TransactionGossip";

const database = Firebase.database();

const generateWalletUser = (user) => {
    var ref = firebase.database().ref("wallet");

    var newPostKey = firebase.database().ref().child('Wallet').push().key;
    var newData = {
                BALANCE: 10000,
                PRIVATE_KEY: "",
                PUBLIC_KEY: "",
                USER_ID: user.uid
    }
    var updates = {};
    updates[newPostKey] = newData;

    database.ref("wallet").update(updates)
} 




const SignUp = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [error, setError] = useState(null);


  const createUserWithEmailAndPasswordHandler = async (event, email, password) => {
    event.preventDefault();
    try{
      const {user} = await auth.createUserWithEmailAndPassword(email, password);
      generateUserDocument(user, {displayName});
      // createUserRealTime(user, {displayName})
      generateWalletUser(user);
    }
    catch(error){
      setError('Error Signing up with email and password');
    }

    setEmail("");
    setPassword("");
    setDisplayName("");
  };


  const onChangeHandler = event => {
    const { name, value } = event.currentTarget;
    if (name === "userEmail") {
      setEmail(value);
    } else if (name === "userPassword") {
      setPassword(value);
    } else if (name === "displayName") {
      setDisplayName(value);
    }
  };

  return (


    <div className="container">
        <div className="boxSignup">
           <div>
         {error !== null && (
          <div>
            {error}
          </div>
        )}</div>
    <form action="/action_page.php">
        <label for="displayName">Nom </label>
        <input value={displayName} type="text" id="displayName" name="displayName" placeholder="Votre nom" onChange={event => onChangeHandler(event)}></input>

        <label for="lemail">Email</label>
        <input value={email} type="email" id="email" name="userEmail" placeholder="Votre email" onChange={event => onChangeHandler(event)}></input>
        <label for="lname">Mot de passe</label>
        <input value={password} type="password" id="password" name="userPassword" placeholder="Votre mot de passe" onChange={event => onChangeHandler(event)}></input>
               <button
               id="submit"

            onClick={event => {
              createUserWithEmailAndPasswordHandler(event, email, password);
            }}
          >
            S'inscrire
          </button>
    </form>
    ou
        <p className="text-center my-3">
          Déjà un compte?{" "}
          <Link to="/">
            Se connecter ici
          </Link>
        </p>

        </div>

    </div>
  );
};
export default SignUp;