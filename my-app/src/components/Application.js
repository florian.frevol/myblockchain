import React, {useContext} from "react";
import { Router } from "@reach/router";
import SignIn from "./SignIn";
import SignUp from "./SignUp";
import ProfilePage from "./ProfilePage";
import HomePage from "../screen/HomePage"
import PasswordReset from "./PasswordReset";
import UserProvider from "../providers/UserProvider";
import {UserContext} from "../providers/UserProvider";

function Application() {
    const user = useContext(UserContext);
  return (
        user ?
        <HomePage />
      :
        <Router>
          <SignUp path="signUp" />
          <SignIn path="/" />
          <PasswordReset path = "passwordReset" />
        </Router>

  );

}
export default Application;