import React, { useContext, useState, useEffect } from 'react'
import Button from '@material-ui/core/Button';
import { UserContext } from "../providers/UserProvider";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Crypt, RSA } from 'hybrid-crypto-js';
import { FaColumns, FaHashtag } from 'react-icons/fa'
import crypto from 'crypto'
// import {NotificationContainer, NotificationManager} from 'react-notifications';



// import {checkNodeNetwork} from './Network'

const firestore = firebase.firestore();
const database = Firebase.database();

const useStyles = makeStyles({
    root: {
        width: '100%',

        // outline: "none"
    },
    container: {
        maxHeight: 440,
        maxWidth: 1300,
        outline: "none",
        border: 0,
    },
    modal: {
        textAlign: 'center',
        width: '35vw',
        backgroundColor: 'white',
        opacity: 0.8,
        outline: 0, // add / remove
    },
    input: {
        width: "100%"
    }
});

const MinerSoftware = () => {
    const classes = useStyles();
    const user = useContext(UserContext)
    const [dataMinerSoftware, setdataMinerSoftware] = useState([]);
    const [toVerifyBlocks, settoVerifyBlocks] = useState([]);

    const [block, setBlock] = useState("");
    const [compute, setCompute] = useState(false);
    const [seconds, setSeconds] = useState(0);
    const [computeHash, setComputeHash] = useState("");
    const [mined, setMined] = useState(false);
    const [dataCompute, setDataCompute] = useState({});
    const [computeHashSend, setComputeHashSend] = useState("");
    const [previousHashSend, setPreviousHashSend] = useState("");
    const [dispayPrevioushash, setDisplayPrevioushash] = useState(false);
    const [nonce, setNonce] = useState(0);
    const [balance, setBalance] = useState(false)
    const [error, setError] = useState(false)
    const [blockChainBolean, setBlockChainBolean] = useState(true)
    const [nbBlockChains, setNbBlockChains] = useState(0)



    const [persoColor_signature, setPersoColorSignature] = useState("white");
    const [persoColor_balance, setPersoColorBalance] = useState("white");
    const [persoColor_coinBase, setPersoColorCoinBase] = useState("white");
    const [persoColor_hash, setPersoColorHash] = useState("white");
    const [persoColor_approvalBlock, setPersoApprovalBlock] = useState("white");
    const [persoColor_previousHash, setPersoColorPreviousHash] = useState("white");
    const [persoColor_FinalButtonDisabled, setPersoColorFinalButtonState] = useState("true");


    useEffect(() => {
        var ref = firebase.database().ref("nodeSoftware/network");
        ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function (snapshot) {
            // console.log(snapshot.key)

            var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/BLOCKCHAIN_LENGTH");
            starCountRef.on('value', (snapshot) => {
                setNbBlockChains(snapshot.val())
                console.log(snapshot.val())
            })
            var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/MinerSoftware");
            starCountRef.on('value', (snapshot) => {

                var newData = [];
                snapshot.forEach((snapshot) => {
                    newData.push(snapshot.val());
                })
                setdataMinerSoftware(newData);
            })
        });
        var refNetwork = firebase.database().ref("nodeSoftware/verifyTransaction");
        refNetwork.orderByChild("user_id").equalTo(user.uid).on("child_added", function (snapshot) {
            var starCountRef = firebase.database().ref("nodeSoftware/verifyTransaction/" + snapshot.key);
            starCountRef.on('value', (snapshot) => {
                var newData = [];
                snapshot.forEach((snapshot) => {
                    newData.push(snapshot.val());
                })
                if (newData.length > 0) {
                    setBlockChainBolean(false)
                }
                console.log(newData)
                settoVerifyBlocks(newData);
            })
        });

        if (compute)
            console.log("woula")
        else
            console.log("pas woula")

        const timer = setTimeout(() => {
            console.log('This will run after 1 second!')
        }, 1000);
        return () => clearTimeout(timer);


    }, [])
    const checkBalance = async () => {
        var ref = firebase.database().ref("wallet");
        // console.log(row.FROM + "\n" + tmp)
        ref.orderByChild("PUBLIC_KEY").equalTo(dataMinerSoftware[0].FROM).on("child_added", function (snapshot) {
            var check = 0;
            dataMinerSoftware.map((row) => {
                check = check + parseInt(row.AMOUNT);
            })
            if (snapshot.val().BALANCE < check) {
                setPersoColorBalance("red");
                setBalance(false)
            }
            else
                setBalance(true)
            // return (false)
        })
        console.log(balance)
    }

    const AddToBlockChain = () => {   // Ajouter une transaction au mempool
        var ref = firebase.database().ref("nodeSoftware/network");
        var crypt = new Crypt();
        var newPostKey = "";
        var newData = {
            BLOCK: toVerifyBlocks[0],
            NONCE: toVerifyBlocks[2],
            DATA: dataMinerSoftware,
            PREV: toVerifyBlocks[3],
            HASH: toVerifyBlocks[1],
            UUID: toVerifyBlocks[5],
            // ENCRYPTED: crypt.encrypt(row.to, row.uuid, signature), 
        }
        
        ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function (snapshot) {
            newPostKey = firebase.database().ref().child('nodeSoftware/network/' + snapshot.key + "/myblockchains").push().key;
            console.log(snapshot.val())
            console.log(snapshot.key)
            var updates = {};
            updates[newPostKey] = newData;
            database.ref('nodeSoftware/network/' + snapshot.key + "/myblockchains").update(updates)
            var hopperRef = ref.child(snapshot.key);
                hopperRef.update({
                    "BLOCKCHAIN_LENGTH": (nbBlockChains + 1)
                });

            var starCountRef = firebase.database().ref("nodeSoftware/verifyTransaction");
            console.log(toVerifyBlocks[5])
            starCountRef.orderByChild("user_id").equalTo(toVerifyBlocks[5]).on('child_added', function (newsnapshot2) {
                console.log(newsnapshot2.key)
                firebase.database().ref('nodeSoftware/verifyTransaction/' + newsnapshot2.key).remove();
            })
            var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/MinerSoftware");
            firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/MinerSoftware/").remove();
            setBlockChainBolean(true)
        });
    }

    function Hash() {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < 20; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }

        result = crypto.createHash('sha1').update(result).digest('hex')

        return result;
    }

    function ChangeColorSignature() {
        if (persoColor_signature == "greenyellow") {
            setPersoColorSignature("white");
        } else {
            setPersoColorSignature("greenyellow");
        }
        CheckFinalButton();
    }

    function ChangeColorBalance() {
        checkBalance()
        if (persoColor_balance == "greenyellow" || persoColor_balance == "red") {
            setPersoColorBalance("white");
        } else if (balance == false) {
            setPersoColorBalance("red");
            setError(true)
        }
        else {
            setPersoColorBalance("greenyellow");
        }
        CheckFinalButton();
    }

    function ChangeColorPreviousHash() {
        if (persoColor_previousHash == "greenyellow") {
            setPersoColorPreviousHash("white")
        }
        else {
            setPersoColorPreviousHash("greenyellow")
        }
        CheckFinalButton();
    }
    function ChangeColorCoinBase() {
        if (persoColor_coinBase == "greenyellow") {
            setPersoColorCoinBase("white");
        } else {
            setPersoColorCoinBase("greenyellow");
        }
        CheckFinalButton();
    }

    function ChangeColorHash() {
        if (persoColor_hash == "greenyellow") {
            setPersoColorHash("white");
        } else {
            setPersoColorHash("greenyellow");
        }
        CheckFinalButton();
    }

    function ChangeColorApprovalBlock() {
        if (persoColor_approvalBlock == "greenyellow") {
            setPersoApprovalBlock("white");
        } else {
            setPersoApprovalBlock("greenyellow");
        }
        CheckFinalButton();
    }

    function CheckFinalButton() {
        if (persoColor_signature == "greenyellow" && persoColor_balance == "greenyellow" && persoColor_coinBase == "greenyellow" && persoColor_hash == "greenyellow" && persoColor_previousHash == "greenyellow" && persoColor_approvalBlock != "greenyellow") {
            setPersoColorFinalButtonState("");
        } else {
            setPersoColorFinalButtonState("false");
        }
    }

    function SendToMyBlockchain() {
        if (persoColor_signature == "greenyellow" && persoColor_balance == "greenyellow" && persoColor_coinBase == "greenyellow" && persoColor_hash == "greenyellow") {
            AddToBlockChain()
        } else {
            // NotificationManager.error('Error message');
        }
    }





    return (
        <div style={{ display: "flex", flexDirection: "column" }}>
            {blockChainBolean
                ? <div style={{ marginTop: 20 }}>
                    <br></br>
                    NEW BLOCK(S) TO VERIFY BEFORE INSERTION IN MY BLOCKCHAIN<br></br>
                 There are no blocks to verify!
                                 </div>
                :
                <div>


                    <div style={{ backgroundColor: mined ? "#91ff35" : "white", padding: 40 }}>
                        <div style={{ display: "flex", alignSelf: "flex-start" }}>Block</div>
                        <div style={{ display: "flex", flexDirection: "row", marginBottom: 30 }}><div style={{ width: "5%", backgroundColor: "#a8ada9" }}> <FaHashtag />  </div>  <input value={toVerifyBlocks[0]} onChange={(event) => setBlock(event.target.value)} className={classes.input}></input> </div>
                        <div style={{ display: "flex", alignSelf: "flex-start" }}>Nonce</div>
                        <input value={toVerifyBlocks[2]} style={{ marginBottom: 30 }} className={classes.input}></input>
                        <div style={{ display: "flex", alignSelf: "flex-start" }}>Data</div>

                        <Paper className={classes.root}>
                            <TableContainer className={classes.container}>
                                <Table stickyHeader aria-label="sticky table">
                                    <TableHead>

                                        <TableRow>
                                            <TableCell align='left'>Amount</TableCell>
                                            <TableCell align='left'>Fee</TableCell>
                                            <TableCell align='left'>From</TableCell>
                                            <TableCell align='left'>To</TableCell>
                                            <TableCell align='left'>Signature</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {dataMinerSoftware.map((row) => (
                                            <TableRow>
                                                <TableCell align='left'>{row.AMOUNT}</TableCell>
                                                <TableCell align='left'>{row.FEE}</TableCell>
                                                <TableCell align='left'>{row.FROM}</TableCell>
                                                <TableCell align='left'>{row.TO}</TableCell>
                                                <TableCell align='left'>{row.SIGNATURE}</TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>

                            </TableContainer>
                        </Paper>
                        <div style={{ display: "flex", alignSelf: "flex-start", marginTop: "30px" }} >Prev</div>
                        <input value={toVerifyBlocks[3]} style={{ marginBottom: 30 }} className={classes.input} disabled ></input>
                        <div style={{ display: "flex", alignSelf: "flex-start" }}>Hash</div>
                        <input value={toVerifyBlocks[1]} style={{ marginBottom: 30 }} className={classes.input} disabled></input>



                        <div style={{ display: "flex", justifyContent: "space-between", flexDirection: "column" }}>

                            <div id="PersoBC_signature" style={{ display: "flex", justifyContent: "space-between", alignItems: "center", padding: "1%", backgroundColor: persoColor_signature }}>
                                <div ><Button variant="contained" color="primary" style={{ width: "12rem" }} onClick={() => { }}>check Signature</Button></div>
                                <div >SIGNATURES OK?</div>
                                <div ><input type="checkbox" onChange={() => { ChangeColorSignature() }} /></div>
                            </div>

                            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row", padding: "1%", backgroundColor: persoColor_balance }}>
                                <div><Button variant="contained" color="primary" style={{ width: "12rem" }} onClick={() => { checkBalance() }}>check balance</Button></div>
                                <div>SUFFICIENT BALANCES OK?</div>
                                <div><input type="checkbox" onChange={() => { ChangeColorBalance() }} /></div>
                            </div>

                            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row", padding: "1%", backgroundColor: persoColor_coinBase }}>
                                <div style={{ width: "12rem" }} ></div>
                                <div>COIN BASE OK?</div>
                                <div><input type="checkbox" onChange={() => { ChangeColorCoinBase() }} /></div>
                            </div>

                            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row", padding: "1%", backgroundColor: persoColor_previousHash }}>
                                <div style={{ width: "12rem" }} ></div>
                                <div>PREVIOUS HASH OK?</div>
                                <div><input type="checkbox" onChange={() => { ChangeColorPreviousHash() }} /></div>
                            </div>

                            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row", padding: "1%", backgroundColor: persoColor_hash }}>
                                <div><Button variant="contained" color="primary" style={{ width: "12rem" }} onClick={() => { }}>check hash</Button></div>
                                <div>HASH OK?</div>
                                <div><input type="checkbox" onChange={() => { ChangeColorHash() }} /></div>
                            </div>

                            <div style={{ display: "flex", justifyContent: "space-between", alignItems: "center", flexDirection: "row", padding: "1%", backgroundColor: persoColor_approvalBlock }}>
                                <div style={{ width: "12rem" }} ></div>
                                <div>MY APPROVAL OF BLOCK</div>
                                <div><input type="checkbox" onChange={() => { ChangeColorApprovalBlock() }} /></div>
                            </div>

                            <div><Button variant="contained" color="primary" style={{ margin: "1%" }} disabled={persoColor_FinalButtonDisabled} onClick={() => { SendToMyBlockchain() }}>Send to my blockchain</Button></div>

                        </div>
                    </div>
                </div>}








            {/* original code  : 

        <div style={{display: "flex", flexDirection: "row", width: "100%", alignItems: "center", justifyContent: "left"}}>
            <label><div style={{display: "flex", alignSelf: "flex-start", marginTop: "10px", marginRight: "370px"}} ><Button variant="contained" color="primary" onClick={() => {}}>check Signature</Button></div></label>
            <label><div style={{display: "flex", alignSelf: "flex-start", marginTop: "10px", marginRight: "380px"}} >SIGNATURES OK?</div></label>
            <label><input type="checkbox" id="signature"/></label>

        </div>
        <div style={{display: "flex", flexDirection: "row", width: "100%", alignItems: "center", justifyContent: "left"}}>
            <label><div style={{display: "flex", alignSelf: "flex-start", marginTop: "10px", marginRight: "380px"}} ><Button variant="contained" color="primary" onClick={() => {}}>check BALANCES</Button></div></label>
            <label><div style={{display: "flex", alignSelf: "flex-start", marginTop: "10px", marginRight: "295px", justifyContent:"center"}} >SUFFICIENT BALANCES OK?</div></label>
            <label><input type="checkbox" id="balance"/></label>
        </div>
        <div style={{display: "flex", flexDirection: "row",  alignItems: "center", justifyContent: "left"}}>
            <label><div style={{display: "flex", alignSelf: "flex-start", marginLeft: "545px", marginRight: "423px"}} >COIN BASE?</div></label>
            <label><input type="checkbox" id="coin"/></label>
        </div>
        <div style={{display: "flex", flexDirection: "row",  alignItems: "center", justifyContent: "center", backgroundColor:"greenyellow"}}>

        <label><div style={{display: "flex", alignSelf: "flex-start"}} >PREVIOUS HASH OK?</div></label>
        </div>

        <div style={{display: "flex", flexDirection: "row", width: "100%", alignItems: "center", justifyContent: "left"}}>
            <label><div style={{display: "flex", alignSelf: "center", marginTop: "10px", marginRight: "420px"}} ><Button variant="contained" color="primary" onClick={() => {}}>check HASH</Button></div></label>
            <label>HASH OK?</label>
        </div>
        <div style={{display: "flex", flexDirection: "column",  alignItems: "center", justifyContent: "center"}}>
            <label><div style={{display: "flex", alignSelf: "flex-start"}} >MY APPROVAL OF BLOCK</div></label>
        </div>

<div style={{display: "flex", marginLeft:"500px"}} ><Button variant="contained" color="primary" onClick={() => {}}>Send to my blockchain</Button></div>*/}

        </div>


    )
}

export default MinerSoftware;