import React, { useContext, useState, useEffect } from 'react'
import { UserContext } from "../providers/UserProvider";
import firebase from "firebase/app";
import "firebase/auth";
import "firebase/firestore";
import Firebase from 'firebase'
import { TabPanel, a11yProps, useStyles } from './Table'
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Button } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const firestore = firebase.firestore();
const database = Firebase.database();



const addItemEconomy = (user, PUBLIC_KEY, PRICE, PRODUCT) => {

  return new Promise(async (resolve, reject) => {

    var bool = false;
    var ref = firebase.database().ref("nodeSoftware/economy");
    ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function (snapshot) {
      bool = true;
    });

    if (!bool) {
      var newPostKey = firebase.database().ref().child('nodeSoftware/economy').push().key;
      var newData = {
        PRICE: PRICE,
        PRODUCT: PRODUCT,
        PUBLIC_KEY: PUBLIC_KEY
      }
      var updates = {};
      updates[newPostKey] = newData;

      database.ref("nodeSoftware/economy").update(updates).then(
        resolve()
      )
    }
  })
}


const useStylesEconomy = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));


const Economy = () => {
  const classes = useStyles();
  const classesEconomy = useStylesEconomy();
  const user = useContext(UserContext);
  const [economy, setEconomy] = useState([]);
  const [PUBLIC_KEY, SET_PUBLIC_KEY] = useState("");
  const [PRODUCT, SET_PRODUCT] = useState("");
  const [PRICE, SET_PRICE] = useState("");

  useEffect(() => {
    var starCountRef = firebase.database().ref('nodeSoftware/economy');
    starCountRef.on('value', (snapshot) => {
      var newData = [];
      snapshot.forEach((snapshot) => {
        newData.push(snapshot.val());
      })
      setEconomy(newData);
      console.log(economy)
    });
  }, [])

  console.log(PUBLIC_KEY)

  return (

    <div>
     
      <div className="section">
        


        <div style={{ display: "flex", flexDirection: "row", width: "100%", alignItems: "center", justifyContent: "center" }}>

          <label>
            Public key :
          <input type="text" value={PUBLIC_KEY} onChange={(event) => SET_PUBLIC_KEY(event.target.value)} />        </label>
          <label>
            Product/Service Offered :
          <input type="text" value={PRODUCT} onChange={(event) => { SET_PRODUCT(event.target.value) }} />        </label>
          <label>
            Price :
          <input type="text" value={PRICE} onChange={(event) => { SET_PRICE(event.target.value) }} />        </label>

          <Button variant="contained" color="primary" onClick={() => {
            addItemEconomy(user, PUBLIC_KEY, PRICE, PRODUCT); SET_PUBLIC_KEY(""); SET_PRODUCT(""); SET_PRICE("")

          }}>Send</Button>
        </div>



        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell align='left'>Public Key</TableCell>
                  <TableCell align='left'>Product/Service Offered</TableCell>
                  <TableCell align='left'>Price</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {economy.map((row) => (
                  <TableRow>
                    <TableCell align='left'>{row.PUBLIC_KEY}</TableCell>
                    <TableCell align='left'>{row.PRODUCT}</TableCell>
                    <TableCell align='left'>{row.PRICE}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>

          </TableContainer>
        </Paper>
      </div>

    </div>
  )
}

export default Economy;