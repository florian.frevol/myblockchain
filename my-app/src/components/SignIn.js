import React, {useState} from "react";
import { Link } from "@reach/router";
import {auth} from '../Firebase'
import '../Styles/Components/signup.css'

const SignIn = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState(null);

    const signInWithEmailAndPasswordHandler = (event,email, password) => {
        event.preventDefault();
        console.log("puttttaiiinnn je veux juste me coooo")
        auth.signInWithEmailAndPassword(email, password).catch(error => {
            setError("Error signing in with password and email!");
            console.error("Error signing in with password and email", error);
        });
      };
      
      const onChangeHandler = (event) => {
          const {name, value} = event.currentTarget;
        
          if(name === 'userEmail') {
              setEmail(value);
          }
          else if(name === 'userPassword'){
            setPassword(value);
          }
      };

      return (


        <div className="container">
        <div className="boxSignup">
           <div>
         {error !== null && (
          <div >
            {error}
          </div>
        )}</div>
    <form action="/action_page.php">

        <label for="lemail">Email</label>
        <input value={email} type="email" id="email" name="userEmail" placeholder="Votre email" onChange={event => onChangeHandler(event)}></input>
        <label for="lname">Mot de passe</label>
        <input value={password} type="password" id="password" name="userPassword" placeholder="Votre mot de passe" onChange={event => onChangeHandler(event)}></input>
               <button
               id="submit"

               onClick = {(event) => {signInWithEmailAndPasswordHandler(event, email, password)}}
          >
            Se connecter
          </button>
    </form>
    ou
        <p className="text-center my-3">
         Pas encore de compte?{" "}
          <Link to="/signUp">
            En créer un ici
          </Link>
        </p>

        </div>

    </div>
      );
};
export default SignIn;