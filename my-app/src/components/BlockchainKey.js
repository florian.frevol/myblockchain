import React, { useContext, useState, useEffect } from "react";
import { auth } from "../Firebase";

import { UserContext } from "../providers/UserProvider";
import { Button, makeStyles, Table, TableBody, TableCell, TableContainer, TableHead } from "@material-ui/core";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

import "firebase/firestore";
import sha256 from 'js-sha256';
import { TabPanel, a11yProps, useStyles } from './Table';

import { Crypt, RSA } from 'hybrid-crypto-js';


const database = Firebase.database();


const getWalletInfo = (user) => {
    return new Promise(async (resolve, reject) => {
        var ref = firebase.database().ref("wallet");
        await ref.orderByChild("USER_ID").equalTo(user.uid).on("child_added", function (snapshot) {
            resolve(snapshot.val())
        });
        reject("error reading");
    })
}

const generateKeyPair = (user) => {

    var rsa = new RSA();


    var ref = firebase.database().ref("wallet");
    ref.orderByChild("USER_ID").equalTo(user.uid).on("child_added", function (snapshot) {
        // var result           = '';
        // var characters       = '0123456789';
        // var charactersLength = characters.length;
        // for ( var i = 0; i < 76; i++ ) {
        //    result += characters.charAt(Math.floor(Math.random() * charactersLength));
        // }
        var publicKey;
        var privateKey;
        rsa.generateKeyPair(function (keyPair) {
            // Callback function receives new key pair as a first argument
            publicKey = keyPair.publicKey;
            privateKey = keyPair.privateKey;
            // console.log(publicKey + " -- " + privateKey);
            var data = snapshot.val();
            data.PUBLIC_KEY = publicKey.replace(/(\r\n|\n|\r)/gm, " ");
            data.PRIVATE_KEY = privateKey.replace(/(\r\n|\n|\r)/gm, " ");
            var updates = {};
            updates[snapshot.key] = data;
            ref.update(updates);
        });
        
        // var hash = sha256(result);
        return (snapshot.val())
    });
}

const BlockChainKeys = () => {
    const classes = useStyles();
    const user = useContext(UserContext)
    const [value, setValue] = React.useState(0);
    const [PUBLIC_KEY, SET_PUBLIC_kEY] = useState("")
    const [PRIVATE_KEY, SET_PRIVATE_kEY] = useState("")
    const [BALANCE, SET_BALANCE] = useState(0)


    useEffect(() => {
        getWalletInfo(user).then((data) => {
            // console.log(data)
            SET_PUBLIC_kEY(data.PUBLIC_KEY);
            SET_PRIVATE_kEY(data.PRIVATE_KEY);
            SET_BALANCE(data.BALANCE)
        })

    })


    return (
        <div >
            <Paper className={classes.root}>
                <TableContainer className={classes.container}>
                    <Table stickyHeader arial-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell align='left'>Public Key</TableCell>
                                <TableCell align='left'>Private key</TableCell>
                                <TableCell align='left'>Balance</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>

                            <TableRow>
                                <TableCell align='left'>{PUBLIC_KEY}<Button variant="contained" color="primary" onClick={() => { navigator.clipboard.writeText(PUBLIC_KEY) }}>Copy</Button></TableCell>
                                <TableCell align='left'>{PRIVATE_KEY}<Button variant="contained" color="primary" onClick={() => { navigator.clipboard.writeText(PRIVATE_KEY) }}>Copy</Button></TableCell>
                                <TableCell align='left'>{BALANCE}</TableCell>
                            </TableRow>
                        </TableBody>

                        <div style={{ height: 80 }}></div>
                        <Button variant="contained" color="primary" onClick={() => {
                            generateKeyPair(user)
                            SET_BALANCE(0)

                        }}>Generate Keys</Button>
                    </Table>
                </TableContainer>
            </Paper>

        </div>
    )
}

export default BlockChainKeys;