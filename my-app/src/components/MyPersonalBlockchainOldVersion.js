import React, { useContext, useState, useEffect } from 'react'
import { UserContext } from "../providers/UserProvider";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import "../Styles/Components/MyBlockChain.css"
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import { Crypt, RSA } from 'hybrid-crypto-js';
import Button from '@material-ui/core/Button';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const database = Firebase.database();

const useStyles = makeStyles({
    root: {
        width: '100%',
    },
    container: {
        maxHeight: 200,
        maxWidth: 1300,
        outline: "none",
        border: 0,
    },
    modal: {
        textAlign: 'center',
        width: '35vw',
        backgroundColor: 'white',
        opacity: 0.8,
        outline: 0, // add / remove
    }
});

var MyPersonalBlockchain = () => {
    const classes = useStyles();

    const user = useContext(UserContext)
    const [blockChain, setBlockChain] = useState([]);
    const [blockChainBolean, setBlockChainBolean] = useState(true)
    const [blockChainData, setBlockChainData] = useState([])
    var temp = []

    useEffect(() => {
        var ref = firebase.database().ref("nodeSoftware/network");
        ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function (snapshot) {
            console.log(snapshot.key)
            var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/myblockchains");
            starCountRef.on('value', (snapshot) => {

                var newData = [];
                snapshot.forEach((snapshot) => {
                    newData.push(snapshot.val());
                })
                newData.forEach((val) => {
                    temp.push(val.DATA)
                })
                setBlockChain(newData);
                setBlockChainData(temp)
                console.log(blockChainData)
                console.log(newData);
                if (newData.length > 0) {
                    setBlockChainBolean(false)
                }
            })
        });

    }, [])

    //   AddToMempool()

    console.log(blockChainBolean)
    return (

        <div style={{ display: "flex", flexDirection: "column", justifyContent: "space-between", marginBottom: "2rem", marginTop: "1rem", maxWidth: 2000 }}>
            <div className="line" style = {{marginBottom : 40}}>
                My blockChain
                <br></br>
            </div>
            {blockChainBolean
                ? <div style={{ alignItems : "flex-start", marginTop: 20 }}>
                    <br></br>
                    You have no blocks in your blockchains!
                </div>
                :
                <div style={{ display: "flex", flexWrap: "wrap", flexDirection: "row" }}>
                    {blockChain.map((row) => (
                        <div style={{ flexDirection: "row", borderRadius: "0.5rem", border: "0.1rem solid Gainsboro", width: "23%", textAlign: "left", fontSize: "1rem", padding: "1%", backgroundColor: "white" }}>

                            Block
                            <div style={{ display: "flex", flexDirection: "row", marginBottom: "0.9rem", marginTop: "0.6rem" }}><div style={{ borderTopLeftRadius: "0.3rem", borderBottomLeftRadius: "0.3rem", width: "10%", backgroundColor: "#E8E8E8", fontSize: "0.6rem", color: "black", textAlign: "center", paddingTop: "3%" }}>  </div>  <input value={row.BLOCK} className={classes.input}></input> </div>
            Nonce
                            <input value={row.NONCE} style={{ marginBottom: "0.9rem", marginTop: "0.6rem", width: "95%" }} ></input>
            Data
                            <Paper className={classes.root}>
                                <TableContainer className={classes.container}>
                                    <Table stickyHeader aria-label="sticky table">
                                        <TableHead>

                                            <TableRow>
                                                <TableCell align='left'>Amount</TableCell>
                                                <TableCell align='left'>Fee</TableCell>
                                                <TableCell align='left'>From</TableCell>
                                                <TableCell align='left'>To</TableCell>
                                                <TableCell align='left'>Signature</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {row.DATA.map((row) => {
                                                console.log(row)
                                                return (
                                                    <TableRow>
                                                        <TableCell align='left'>{row.AMOUNT}</TableCell>
                                                        <TableCell align='left'>{row.FEE}</TableCell>
                                                        <TableCell align='left'>{row.FROM}</TableCell>
                                                        <TableCell align='left'>{row.TO}</TableCell>
                                                        <TableCell align='left'>{row.SIGNATURE}</TableCell>
                                                    </TableRow>
                                                )
                                            })}
                                        </TableBody>
                                    </Table>

                                </TableContainer>
                            </Paper>
                            <div style={{ display: "flex", alignSelf: "flex-start", marginTop: "30px" }} >Prev</div>
                            <input value={row.PREV} style={{ marginBottom: "0.9rem", marginTop: "0.6rem", width: "95%" }} className={classes.input} disabled ></input>
                            <div style={{ display: "flex", alignSelf: "flex-start" }}>Hash</div>
                            <input value={row.HASH} style={{ marginBottom: "0.9rem", marginTop: "0.6rem", width: "95%" }} disabled></input>

                        </div>))}
                </div>}

        </div>

        /*<div className="container" style={{ width: 1300, height: 400, borderRadius: 20, flexDirection: "column", justifyContent: "flex-start", alignItems: "flex-start" }}>
             <Button variant="contained" color="primary" onClick={() => AddToMempool()} >Send to mempool</Button><div style={{ marginBottom: 20 }}>
                 NEW BLOCK(S) TO VERIFY BEFORE INSERTION IN MY BLOCKCHAIN<br></br>
                 There are no blocks to verify!
             </div>
             <div className="line">
                 <br></br>
             </div>
             <div>
                 MY BLOCKCHAIN
             </div>
             {blockChainBolean
                 ? <div style={{ marginTop: 20 }}>
                     <br></br>
                     You have no blocks in your blockchains!
                 </div>
                 :
                 <div>
                     {blockChain.map((row) => (
                         <div style={{ flexDirection : "row", backgroundColor: "grey", height: 20 }}>
                             wsh
                             <div style={{ backgroundColor: "grey", height: 20 }}>
                                 <TableContainer className={classes.container}>
                                     <Table stickyHeader aria-label="sticky table">
                                         <TableHead>
 
                                             <TableRow>
                                                 <TableCell align='left'>Amount</TableCell>
                                                 <TableCell align='left'>Fee</TableCell>
                                                 <TableCell align='left'>From</TableCell>
                                                 <TableCell align='left'>To</TableCell>
                                                 <TableCell align='left'>Signature</TableCell>
                                             </TableRow>
                                         </TableHead>
                                         <TableBody>
                                                 <TableRow>
                                                     <TableCell align='left'>{row.AMOUNT}</TableCell>
                                                     <TableCell align='left'>{row.FEE}</TableCell>
                                                     <TableCell align='left'>{row.FROM}</TableCell>
                                                     <TableCell align='left'>{row.TO}</TableCell>
                                                     <TableCell align='left'>{row.SIGNATURE}</TableCell>
                                                 </TableRow>
                                         </TableBody>
                                     </Table>
 
                                 </TableContainer>
                             </div>
                         </div>
                     ))}
 
                 </div>}
         </div>*/
    )
}


export default MyPersonalBlockchain