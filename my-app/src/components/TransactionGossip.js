import React, { useContext, useState, useEffect } from 'react'
import Button from '@material-ui/core/Button';
import { UserContext } from "../providers/UserProvider";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Crypt, RSA } from 'hybrid-crypto-js';

// import {checkNodeNetwork} from './Network'

const firestore = firebase.firestore();
const database = Firebase.database();

const useStyles = makeStyles({
  root: {
    width: '100%',
    outline: "none"
  },
  container: {
    maxHeight: 440,
    maxWidth: 1300,
    outline: "none",
    border: 0,
  },
  modal: {
    textAlign: 'center',
    width: '35vw',
    backgroundColor: 'white',
    opacity: 0.8,
    outline: 0, // add / remove
  }
});


// const addTansactionGossip = (user) => {
//   return new Promise((resolve, reject) => {

//     var newPostKey = firebase.database().ref().child('nodeSoftware/transactionGossip').push().key;

//     var newData = {
//       AMOUNT: 0,
//       FEE: 0,
//       FROM: "",
//       TO: "",
//       SIGNATURE: "",
//     }

//     var updates = {};
//     updates[newPostKey] = newData;

//     database.ref("nodeSoftware/transactionGossip").update(updates).then(
//       resolve()
//     )
//   }
//   )

// }
// const theme = createMuiTheme({
//   palette: {
//     primary: green,
//   },
// });

const TransactionGossip = () => {
  const classes = useStyles();
  const user = useContext(UserContext)
  const [transactionsGossip, setTransactionGossip] = useState([]);



  useEffect(() => {
    var ref = firebase.database().ref("nodeSoftware/network");
    ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(snapshot) {
      // console.log(snapshot.key)
      var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/transactionsGossip");
    starCountRef.on('value', (snapshot) => {

      var newData = [];
        snapshot.forEach((snapshot) => {
          newData.push(snapshot.val());
        })
        setTransactionGossip(newData);
    })
    });

  }, [])


  // console.log(transactionsGossip)

  const checkBalance = async (row) => {
    var ref = firebase.database().ref("wallet");
    // console.log(row.FROM + "\n" + tmp)
    ref.orderByChild("PUBLIC_KEY").equalTo(row.FROM).on("child_added", function (snapshot) {
      // console.log(row.key)
      // if (snapshot.BALANCE < row.BALANCE) {
        var refNetwork = firebase.database().ref("nodeSoftware/network")
        refNetwork.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(newsnapshot) {
          // console.log("snapshot key: " + snapshot.key + "newsnapshot key: "  + newsnapshot.key)
          var starCountRef = firebase.database().ref('nodeSoftware/network/' + newsnapshot.key + "/transactionsGossip");
          starCountRef.orderByChild("UUID").equalTo(row.UUID).on('child_added', function(newsnapshot2) {
            console.log('nodeSoftware/network/' + newsnapshot.key + "/transactionsGossip/" + newsnapshot2.key)
            var test = firebase.database().ref('nodeSoftware/network/' + newsnapshot.key + "/transactionsGossip/" + newsnapshot2.key)
            if (snapshot.val().BALANCE < row.AMOUNT) {              
              test.child("BALANCE_OK").set("1") //NON

            } else {
              test.child("BALANCE_OK").set("0") //OUI
            }
          })
        });
      // }
      

    })
    // return (false)
  }

  const checkSignature = (row) => {
    var ref = firebase.database().ref("wallet");
    // console.log(row.FROM + "\n" + tmp)
    var crypt = new Crypt();

    
    
    var refNetwork = firebase.database().ref("nodeSoftware/network")
    refNetwork.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(newsnapshot) {
      // console.log("snapshot key: " + snapshot.key + "newsnapshot key: "  + newsnapshot.key)
      var starCountRef = firebase.database().ref('nodeSoftware/network/' + newsnapshot.key + "/transactionsGossip");
      starCountRef.orderByChild("UUID").equalTo(row.UUID).on('child_added', function(newsnapshot2) {
        console.log('nodeSoftware/network/' + newsnapshot.key + "/transactionsGossip/" + newsnapshot2.key)
        var test = firebase.database().ref('nodeSoftware/network/' + newsnapshot.key + "/transactionsGossip/" + newsnapshot2.key)
          test.child("SIGNATURE_OK").set("0") 
        // if (snapshot.val().BALANCE < row.AMOUNT) {              
        //   test.child("BALANCE_OK").set("1") //NON

        // } else {
        //   test.child("BALANCE_OK").set("0") //NON
        // }
      })
    });

    

    // console.log("userid: " + user.uid)
    // ref.orderByChild("PUBLIC_KEY").equalTo(row.FROM).on("child_added", function (snapshot) {
    //   console.log(snapshot.val())
    //   var test = new Crypt();
    //   var encrypted = crypt.signature(snapshot.val().PRIVATE_KEY, "message");
    //   console.log(encrypted)
    //   var decrypted = crypt.decrypt(snapshot.val().PRIVATE_KEY, encrypted);
    //   var message = decrypted.message;
    //   console.log(message)
    // })


 
  }

  const AddToMempool = (row) => {   // Ajouter une transaction au mempool
    var ref = firebase.database().ref("nodeSoftware/network");
    var crypt = new Crypt();
    var newPostKey = "";
    console.log(row);
    // var signature = crypt.signature(row.privateKey, row.uuid);
    var newData = {
            AMOUNT: row.AMOUNT,
            FEE: row.FEE,
            FROM: row.FROM,
            TO: row.TO,
            SIGNATURE: row.SIGNATURE,
            UUID: row.UUID,
            // ENCRYPTED: crypt.encrypt(row.to, row.uuid, signature), 
    }
    ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(snapshot) {
        newPostKey = firebase.database().ref().child('nodeSoftware/network/' + snapshot.key + "/mempool").push().key;
        console.log(snapshot.val())
        console.log(snapshot.key)
        var updates = {};
        updates[newPostKey] = newData;
        database.ref('nodeSoftware/network/' + snapshot.key + "/mempool").update(updates)



        var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/transactionsGossip");
        starCountRef.orderByChild("UUID").equalTo(row.UUID).on('child_added', function(newsnapshot2) {
          // console.log('nodeSoftware/network/' + snapshot.key + "/transactionsGossip/" + newsnapshot2.key)
          firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/transactionsGossip/" + newsnapshot2.key).remove();

          // var test = firebase.database().ref('nodeSoftware/network/' + newsnapshot.key + "/transactionsGossip/" + newsnapshot2.key)
          //   test.child("SIGNATURE_OK").set("0") 
          // if (snapshot.val().BALANCE < row.AMOUNT) {              
          //   test.child("BALANCE_OK").set("1") //NON
  
          // } else {
          //   test.child("BALANCE_OK").set("0") //NON
          // }
        })



    });

    


  }


  return (
    <div>
      {/* <Button variant="contained" color="primary" onClick={() => addTansactionGossip(user).then((doc) => {console.log(doc)}).catch(err => console.log(err))}>Add transaction gossip</Button> */}
      {/* {transactionsGossip[0].FROM} */}
      <Paper className={classes.root}>
        <TableContainer className={classes.container}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>

              <TableRow>
                <TableCell align='left'>Amount</TableCell>
                <TableCell align='left'>Fee</TableCell>
                <TableCell align='left'>From</TableCell>
                <TableCell align='left'>Signature</TableCell>
                <TableCell align='left'>To</TableCell>
                <TableCell align='left'>Balance OK?</TableCell>
                <TableCell align='left'>Signature OK?</TableCell>
                <TableCell align='left'>Send to Mempool</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {transactionsGossip.map((row) => (
                <TableRow>
                  <TableCell align='left'>{row.AMOUNT}</TableCell>
                  <TableCell align='left'>{row.FEE}</TableCell>
                  <TableCell align='left'>{row.FROM}</TableCell>
                  <TableCell align='left'>{row.SIGNATURE}</TableCell>
                  <TableCell align='left'>{row.TO}</TableCell>
                  {/* {row.BALANCE_OK == "2" ? <Button variant="contained" disabled>pas bougé</Button> : row.BALANCE_OK == "0" ? <Button variant="contained" color="primary">Oui</Button> : <Button variant="contained" color="secondary">Non</Button>} */}
              <TableCell align='left'>{row.BALANCE_OK == "2" ? <Button variant="contained" color="primary" onClick={() => checkBalance(row)}>Check Balance</Button> : row.BALANCE_OK == "0" ? <Button variant="contained" >OK</Button> : <Button variant="contained" color="secondary">NOK</Button>}</TableCell>
                  <TableCell align='left'>{row.SIGNATURE_OK == "2" ? <Button variant="contained" color="primary" onClick={() => checkSignature(row)}>Check Signature</Button> : row.SIGNATURE_OK == "0" ? <Button variant="contained" >OK</Button> : <Button variant="contained" color="secondary">NOK</Button>}</TableCell>
                  <TableCell align='left'>{(row.BALANCE_OK == '0' && row.SIGNATURE_OK == '0') ?<Button  variant="contained" color="primary" onClick={() => AddToMempool(row)} >Send to mempool</Button> : <Button variant="contained" disabled>Send to mempool</Button> }        </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>

        </TableContainer>
      </Paper>
      {/* <ThemeProvider theme={theme}>
        <Button variant="contained" color="primary">
          OK
        </Button>
      </ThemeProvider> */}
    </div>
  )
}

export default TransactionGossip;