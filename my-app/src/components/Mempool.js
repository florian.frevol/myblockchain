import React, { useContext, useState, useEffect } from 'react'
import Button from '@material-ui/core/Button';
import { UserContext } from "../providers/UserProvider";
import firebase from "firebase/app";
import Firebase from 'firebase'
import "firebase/auth";
import "firebase/firestore";
import { createMuiTheme, makeStyles, ThemeProvider } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Crypt, RSA } from 'hybrid-crypto-js';

// import {checkNodeNetwork} from './Network'

const firestore = firebase.firestore();
const database = Firebase.database();



const useStyles = makeStyles({
  root: {
    width: '100%',
    outline: "none"
  },
  container: {
    maxHeight: 440,
    maxWidth: 1300,
    outline: "none",
    border: 0,
  },
  modal: {
    textAlign: 'center',
    width: '35vw',
    backgroundColor: 'white',
    opacity: 0.8,
    outline: 0, // add / remove
  }
});

const Mempool = () => {
    const classes = useStyles();
    const user = useContext(UserContext)
    const [transactionsGossip, setTransactionGossip] = useState([]);
  

    const AddToMiner = (row) => {   // Ajouter une transaction au Miner
      var ref = firebase.database().ref("nodeSoftware/network");
      var newPostKey = "";
      var newData = {
              AMOUNT: row.AMOUNT,
              FEE: row.FEE,
              FROM: row.FROM,
              TO: row.TO,
              SIGNATURE: row.SIGNATURE,
              UUID: row.UUID,
      }
      ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(snapshot) {
          newPostKey = firebase.database().ref().child('nodeSoftware/network/' + snapshot.key + "/MinerSoftware").push().key;
          var updates = {};
          updates[newPostKey] = newData;
          database.ref('nodeSoftware/network/' + snapshot.key + "/MinerSoftware").update(updates)
    
          console.log("ohhh ptn")
    
          var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/mempool");
          starCountRef.orderByChild("UUID").equalTo(row.UUID).on('child_added', function(newsnapshot2) {
            firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/mempool/" + newsnapshot2.key).remove();
          })
        });
    }
  
  
    useEffect(() => {
      var ref = firebase.database().ref("nodeSoftware/network");
      ref.orderByChild("ID_USER").equalTo(user.uid).on("child_added", function(snapshot) {
        // console.log(snapshot.key)
        var starCountRef = firebase.database().ref('nodeSoftware/network/' + snapshot.key + "/mempool");
      starCountRef.on('value', (snapshot) => {
  
        var newData = [];
          snapshot.forEach((snapshot) => {
            newData.push(snapshot.val());
          })
          setTransactionGossip(newData);
      })
      });
  
    }, [])
  
    
  
    return (
      <div>
        {/* <Button variant="contained" color="primary" onClick={() => addTansactionGossip(user).then((doc) => {console.log(doc)}).catch(err => console.log(err))}>Add transaction gossip</Button> */}
        {/* {transactionsGossip[0].FROM} */}
        <Paper className={classes.root}>
          <TableContainer className={classes.container}>
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
  
                <TableRow>
                  <TableCell align='left'>Amount</TableCell>
                  <TableCell align='left'>Fee</TableCell>
                  <TableCell align='left'>From</TableCell>
                  <TableCell align='left'>To</TableCell>
                  <TableCell align='left'>Signature</TableCell>
                  <TableCell align='left'>INSERT TRANSACTION INTO NEW BLOCK TO BE MINED</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {transactionsGossip.map((row) => (
                  <TableRow>
                    <TableCell align='left'>{row.AMOUNT}</TableCell>
                    <TableCell align='left'>{row.FEE}</TableCell>
                    <TableCell align='left'>{row.FROM}</TableCell>
                    <TableCell align='left'>{row.TO}</TableCell>
                    <TableCell align='left'>{row.SIGNATURE}</TableCell>
                    <TableCell align='left'><Button onClick={() => AddToMiner(row)} variant="contained" color="primary"  >INSERT TRANSACTION INTO NEW BLOCK TO BE MINED</Button></TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
  
          </TableContainer>
        </Paper>
      </div>
    )
  }

  export default Mempool;